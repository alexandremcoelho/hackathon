

hackathon_1 = ["Team Kenzie", "Team Ateliware", "Team VHSYS", "Team Mirum"]
hackathon_2 = ["Team Ateliware", "Team Kenzie", "Team VHSYS", "Team Mirum"]
hackathon_3 = ["Team Mirum", "Team Ateliware","Team VHSYS", "Team Kenzie"]

def get_score(team_name, teams):
    output =  1
    for team in teams:
        if team != team_name:
            output += 1
        else:
            return (f'A Team {team_name} ficou classificada em {output}')
    

get_score_1 = get_score("Team Ateliware", hackathon_1)
print(get_score_1)


get_score_2 = get_score("Team Kenzie", hackathon_1)
print(get_score_2)


get_score_3 = get_score("Team Mirum", hackathon_3)
print(get_score_3)

get_score_4 = get_score("Team Kenzie", hackathon_3)
print(get_score_4)
